(use-modules (guix packages)
             (guix licenses)
             (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages package-management)
             (gnu packages gnupg))

(define libgcrypt
  (package (inherit libgcrypt)
    (native-search-paths
     (list (search-path-specification
            (variable "LTDL_LIBRARY_PATH")
            (files '("lib")))))))

(package
  (name "guix-web")
  (version "0.1")
  (source #f)
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)))
  (inputs
   `(("guile" ,guile-2.0)
     ("guile-json" ,guile-json)
     ("guix" ,guix)
     ("libgcrypt" ,libgcrypt)))
  (synopsis "Web frontend for GNU Guix")
  (description "Guix-web is a web interface to the GNU Guix package
manager written in GNU Guile Scheme and JavaScript.")
  (home-page "https://gitorious.org/guix-web/guix-web")
  (license agpl3+))
