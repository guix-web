;;; guix-web - Web interface for GNU Guix
;;; Copyright © 2014  David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (tests guix-web util)
  #:use-module (srfi srfi-64)
  #:use-module (web request)
  #:use-module (web uri)
  #:use-module (guix-web util))

(test-begin "util")

(test-equal "parse-query-string"
  '(("foo" . "1") ("bar" . "hello world"))
  (parse-query-string "foo=1&bar=hello%20world"))

(test-equal "request-path-components"
  '("foo" "bar" "baz.html")
  (request-path-components
   (build-request (string->uri "http://127.0.0.1/foo/bar/baz.html"))))

(test-equal "file-extension"
  "js"
  (file-extension "foo.js"))

(test-group "directory?"
  (test-assert "when file is a directory"
    (not (directory? (getcwd))))

  (test-assert "when file is not a directory"
    (not (directory? (current-filename)))))

(test-end)

(exit (= (test-runner-fail-count (test-runner-current)) 0))
