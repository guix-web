Guix-web
========

A web interface to the GNU Guix package manager written in GNU Guile
and JavaScript.

![guix-web screenshot](http://media.dthompson.us/mgoblin_media/media_entries/20/Screenshot_from_2014-08-13_180724.png)

Features
--------

* Convenient package search
* Detailed package descriptions
* JSON API
* [LibreJS](https://gnu.org/software/librejs/) compatible

Wishlist
--------

* Secure user authentication
* Package installation/removal/rollback
* Profile viewer
* Optional JavaScript minification
* Automated AGPL compliance

Use
---

To run guix-web from the root of the source tree, simply run `guix
web` and visit `localhost:8080` in your web browser.

Dependencies
------------

* [GNU Guile](https://gnu.org/s/guile) >= 2.0.5
* [GNU Guix](https://gnu.org/s/guix) >= 0.6
* [guile-json](https://github.com/aconchillo/guile-json) >= 0.4.0

Development
-----------

To create a development environment, run `guix environment -l
env.scm`.  Within that environment, run `./autogen.sh && ./configure
&& make`.

Copyright
---------

guix-web - Web interface for GNU Guix

Copyright © 2014  David Thompson <davet@gnu.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program.  If not, see
<http://www.gnu.org/licenses/>.
