// guix-web - Web interface for GNU Guix
// Copyright © 2014  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.



(function() {
  var packages = guix.packages = {};

  packages.Packages = _.once(function() {
    return m.request({
      method: "GET",
      url: "packages.json",
      background: true
    });
  });

  packages.PackagesByName = function(name) {
    return m.request({
      method: "GET",
      url: "/package/".concat(name).concat(".json"),
      background: true
    });
  };

  packages.installPackage = function(package) {
    return m.request({
      method: "POST",
      url: "/packages/"
        .concat(package.name)
        .concat("/install"),
      background: true
    });
  };

  packages.PHASE_NONE = 0;
  packages.PHASE_PROMPT = 1;
  packages.PHASE_DERIVATION = 2;
  packages.PHASE_SUCCESS = 3;
  packages.PHASE_ERROR = 4;
})();
