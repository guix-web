// guix-web - Web interface for GNU Guix
// Copyright © 2014  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

var guix = {};

// Shorthand for Kefir module.
var K = Kefir;

// Here is a perfect example of why Scheme is better than JavaScript:
// + is an operator, not a function.  So if I want to, say, compute
// the sum of an array of numbers, I need to write a wrapper function.
guix.add = function(x, y) {
  return x + y;
};

guix.clamp = function(n, min, max) {
  return Math.max(Math.min(n, max), min);
};

guix.chunk = function(array, size) {
  return array.reduce(function(memo, value, i) {
    var currentSlice = _(memo).last();

    if(i / size < memo.length) {
      currentSlice.push(value);
    } else {
      memo.push([value]);
    }

    return memo;
  }, []);
};

// Mithril + Kefir integration
guix.withEmit = function(emitter) {
  return emitter.emit.bind(emitter);
};

guix.withEmitAttr = function(attr, emitter) {
  return m.withAttr(attr, guix.withEmit(emitter));
};

guix.makeModule = function(controller) {
  var view = m.prop([]);

  // Cheat the module system a bit.
  return {
    controller: function() {
      controller().onValue(function(newView) {
        m.startComputation();
        view(newView);
        m.endComputation();
      });
    },
    view: function() {
      return view();
    }
  };
};
