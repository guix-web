// guix-web - Web interface for GNU Guix
// Copyright © 2015  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

(function() {
  guix.packageInfo = {};

  guix.packageInfo.controller = function() {
    var name = m.route.param("name");
    var packages = K.fromPromise(guix.packages.PackagesByName(name));

    // View.
    return guix.ui.spinUntil(packages.map(function(packages) {
      var packageCount = (function() {
        var count = packages.length;
        var units = count > 1 ? " versions" : " version";

        return count.toString().concat(units);
      })();

      function describeInputs(inputs, description) {
        return _.isEmpty(inputs) ? [] : [
          m("dt", description),
          m("dd", m("ul", inputs.map(function(p) {
            return m("li", m("a", {
              config: m.route,
              href: "/package/".concat(p.name)
            }, p.name.concat(" ").concat(p.version)));
          })))
        ];
      }

      function describePackage(package) {
        var baseDescription = [
          m("dt", "Version"),
          m("dd", package.version),
          m("dt", "Synopsis"),
          m("dd", package.synopsis),
          m("dt", "Description"),
          m("dd", package.description),
          m("dt", "License"),
          m("dd", guix.ui.licenseList(package))
        ];
        var inputs = describeInputs(package.inputs, "Inputs");
        var nativeInputs = describeInputs(package.nativeInputs,
                                          "Native Inputs");
        var propagatedInputs = describeInputs(package.propagatedInputs,
                                              "Propagated Inputs");
        return m("li", m("dl", _.flatten([
          baseDescription,
          inputs,
          nativeInputs,
          propagatedInputs
        ], true)));
      }

      return [
        guix.ui.headerWithBadge(name, packageCount),
        m("ul.list-unstyled", packages.map(describePackage))
      ];
    })).map(guix.withLayout);
  };
})();
