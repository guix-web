// guix-web - Web interface for GNU Guix
// Copyright © 2014  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

guix.generations.controller = function() {
  var generations = K.fromPromise(guix.generations.Generations());
  var sortedGenerations = generations.map(function(generations) {
    return _.sortBy(generations, function(g) {
      return -g.number;
    });
  });

  return guix.ui.spinUntil(sortedGenerations.map(function(generations) {
      return [
        guix.ui.headerWithBadge("Generations", generations.length),
        m("table.table.table-bordered", [
          m("thead", m("tr", [
            m("th", "#"),
            m("th", "Name"),
            m("th", "Version"),
            m("th", "Output"),
            m("th", "Location")
          ])),
          m("tbody", [
            generations.map(function(generation) {
              var entries = generation.manifestEntries;

              function renderRow(entry, isFirst) {
                return m("tr", [
                  isFirst ? m("td", {
                    rowspan: entries.length
                  }, m("strong", generation.number)) : null,
                  m("td", entry.name),
                  m("td", entry.version),
                  m("td", entry.output),
                  m("td", entry.location)
                ]);
              }

              return [renderRow(entries[0], true)]
                .concat(entries.slice(1).map(function (entry) {
                  return renderRow(entry, false);
                }));
            })
          ])
        ])
      ];
  })).map(guix.withLayout);
};
