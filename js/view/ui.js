// guix-web - Web interface for GNU Guix
// Copyright © 2015  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.
(function() {
  var ui = guix.ui = {};

  ui.modal = function(title, body, footer) {
    return [
      m(".modal-backdrop.in"),
      m("div.modal.modal-open", {
        style: {
          display: "block"
        }
      }, m(".modal-dialog", [
        m(".modal-content", [
          m(".modal-header", [
            m("h4.modal-title", title)
          ]),
          m(".modal-body", body),
          m(".modal-footer", footer)
        ])
      ]))
    ];
  };

  ui.headerWithBadge = function(title, badgeText) {
    return m("h2.header", [
      title,
      m("span.badge", badgeText)
    ]);
  };

  ui.licenseList = function(package) {
    function licenseLink(license) {
      return m("a", { href: license.uri }, license.name);
    }

    if(_.isArray(package.license)) {
      return m("ul.list-inline", package.license.map(function(license) {
        return m("li", licenseLink(license));
      }));
    } else if(package.license) {
      return licenseLink(package.license);
    } else {
      return "";
    }
  };

  ui.paginate = function(currentPage, numPages, maxShown, emitter) {
    function renderPage(text, attrs) {
      attrs = attrs || {};
      return m("li", attrs, m("a", { href: "#" }, text));
    }

    var ellipsis = renderPage("…", { class: "disabled" });
    var start = currentPage - currentPage % maxShown;
    var lastPage = numPages - 1;
    var firstPrevClass = currentPage === 0 ? "disabled" : "";
    var lastNextClass = currentPage === lastPage ? "disabled" : "";
    var range = _.range(start, Math.min(start + maxShown, numPages));

    return m("div", m("ul.pagination", [
      // Back page
      renderPage("First", {
        class: firstPrevClass,
        onclick: function() {
          emitter.emit(0);
        }
      }),
      // Jump to first page
      renderPage("Previous", {
        class: firstPrevClass,
        onclick: function() {
          if(currentPage > 0) {
            emitter.emit(currentPage - 1);
          }
        }
      }),
      // Display ellipsis if there are hidden pages.
      start > 0 ? ellipsis : ""
    ].concat(range.map(function(i) {
      // Jump to page
      var attrs = {
        class: i === currentPage ? "active" : "",
        onclick: function() {
          emitter.emit(i);
        }
      };
      return renderPage(i + 1, attrs);
    })).concat([
      // Display ellipsis if there are hidden pages.
      start + maxShown < numPages ? ellipsis : "",
      // Forward page
      renderPage("Next", {
        class: lastNextClass,
        onclick: function() {
          if(currentPage < lastPage) {
            emitter.emit(currentPage + 1);
          }
        }
      }),
      // Jump to last page
      renderPage("Last", {
        class: lastNextClass,
        onclick: function() {
          emitter.emit(lastPage);
        }
      })
    ])));
  };

  ui.spinner = m(".spinner", [
    m(".rect1"),
    m(".rect2"),
    m(".rect3"),
    m(".rect4"),
    m(".rect5")
  ]);

  ui.spinUntil = function(ob) {
    return K.merge([K.constant(ui.spinner), ob]);
  };
})();
