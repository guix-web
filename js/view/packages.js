// guix-web - Web interface for GNU Guix
// Copyright © 2014, 2015  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

(function() {
  var view = guix.packages.view = {};

  view.installModal = function(package, phase, phaseStream) {
    function renderPromptModal() {
      var body = [
        m("p", "Do you want to install the following packages?"),
        m("ul", [
          m("li", [
            package.name,
            " ",
            package.version
          ])
        ])
      ];
      var buttons = [
        m(".btn.btn-default", {
          onclick: guix.withEmit(phaseStream, guix.packages.PHASE_NONE)
        }, "Cancel"),
        m(".btn.btn-primary", {
          onclick: function() {
            phaseStream.emit(guix.packages.PHASE_DERIVATION);
            guix.packages.installPackage(package).then(function() {
              phaseStream.emit(guix.packages.PHASE_SUCCESS);
            }, function() {
              phaseStream.emit(guix.packages.PHASE_ERROR);
            });
          }
        }, "Install"),
      ];

      return guix.ui.modal("Install Packages", body, buttons);
    }

    function renderDerivationModal() {
      var body = [
        m("p", [
          "Installing ",
          package.name,
          " ",
          package.version,
          "..."
        ]),
        m(".progress", [
          m(".progress-bar.progress-bar-striped.active", {
            role: "progressbar",
            style: { width: "100%" }
          })
        ])
      ];
      var buttons = m(".btn.btn-danger", {
        onclick: guix.withEmit(phaseStream, guix.packages.PHASE_NONE)
      }, "Abort");

      return guix.ui.modal("Install Packages", body, buttons);
    }

    function renderSuccessModal() {
      var body = m(".alert.alert-success", "Installation complete!");
      var buttons = m(".btn.btn-primary", {
        onclick: guix.withEmit(phaseStream, guix.packages.PHASE_NONE)
      }, "Close");

      return guix.ui.modal("Install Packages", body, buttons);
    }

    function renderErrorModal() {
      var body = m(".alert.alert-danger", "Installation failed!");
      var buttons = m(".btn.btn-primary", {
        onclick: guix.withEmit(phaseStream, guix.packages.PHASE_NONE)
      }, "Close");

      return guix.ui.modal("Install Packages", body, buttons);
    }

    switch(phase) {
    case guix.packages.PHASE_PROMPT:
      return renderPromptModal();
    case guix.packages.PHASE_DERIVATION:
      return renderDerivationModal();
    case guix.packages.PHASE_SUCCESS:
      return renderSuccessModal();
    case guix.packages.PHASE_ERROR:
      return renderErrorModal();
    }

    return null;
  };
})();
