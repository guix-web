// guix-web - Web interface for GNU Guix
// Copyright © 2014  David Thompson <davet@gnu.org>
//
// This program is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

var guix = guix || {};

guix.withLayout = (function() {
  var footer = m("footer", m("small.text-center", [
    m("p", [
      "Made with ",
      m("strong", "λ"),
      " by the GNU Guix hackers — Copyright © 2014, 2015"
    ]),
    m("ul.list-inline", [
      m("li", [
        "Guix-web is licensed under the ",
        m("a", {
          href: "http://www.gnu.org/licenses/agpl-3.0.html"
        }, "GNU Affero General Public License version 3 or later")
      ]),
      m("li", [
        m("a", {
          href: "https://getbootstrap.com"
        }, "Bootstrap CSS"),
        " is licensed under the ",
        m("a", {
          href: "https://directory.fsf.org/wiki/License:Apache2.0"
        }, "Apache 2.0 license")
      ]),
      m("li",
        m("a", { href: "/librejs" }, "JavaScript license information"))
    ])
  ]));

  return function(elem) {
    if(!_.isArray(elem)) {
      elem = [elem];
    }

    return [
      m("nav.navbar.navbar-default.navbar-static-top", {
        role: "navigation"
      }, m(".container", [
        m(".navbar-header", m("img.logo", { src: "/images/logo.png" })),
        m("ul.nav.navbar-nav", [
          m("li.active", m("a", {
            config: m.route,
            href: "/"
          }, "Packages")),
          m("li", m("a", {
            config: m.route,
            href: "/generations"
          }, "Generations"))
        ]),
        m("ul.nav.navbar-nav.navbar-right")
      ])),
      m(".container", elem.concat([footer]))
    ];
  };
})();
