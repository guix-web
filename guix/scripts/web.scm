;;; guix-web - Web interface for GNU Guix
;;; Copyright © 2014, 2015  David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix scripts web)
  #:use-module (system repl server)
  #:use-module (guix web controller)
  #:use-module (guix web server)
  #:export (guix-web))

(define (guix-web . args)
  (spawn-server (make-tcp-server-socket #:port 37146))
  (start-guix-web (lambda (path) (controller path))))
