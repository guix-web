;;; guix-web - Web interface for GNU Guix
;;; Copyright © 2014  David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix web controller)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web request)
  #:use-module (gnu packages)
  #:use-module (guix web render)
  #:use-module (guix web view html)
  #:use-module (guix web view json)
  #:use-module (guix web package)
  #:export (controller))

(define extract-package-name
  (let ((regexp (make-regexp "^(.*)\\.json$")))
    (lambda (path-part)
      (and=> (regexp-exec regexp path-part)
             (cut match:substring <> 1)))))

(define controller
  (match-lambda
   ((GET)
    (redirect '("packages")))
   ((GET "packages")
    (render-html (all-packages)))
   ((GET "packages.json")
    (render-json (all-packages-json)))
   ((GET "package" (= extract-package-name (? string? name)))
    (render-json (view-package-json name)))
   ((POST "packages" name "install")
    (let ((package (car (find-packages-by-name name))))
      (if (package-install package)
          (created)
          (unprocessable-entity))))
   ((GET "generations")
    (render-json (generations-json)))
   ((GET "librejs")
    (render-html (librejs)))
   ((GET path ...)
    (render-static-asset path))))
